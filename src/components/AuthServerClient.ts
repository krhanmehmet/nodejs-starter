import {HttpClient} from '../utils/httpclient';

export class AuthServerClient {

     static url : "http://localhost:8765/uaa/users/current";

    public static getOAuth(accessToken : string) : Promise<any> {
       return HttpClient.get('http://localhost:8765/uaa/users/current',{
            headers : {
                Authorization : accessToken
            }
        })
    }

    public static getUserByToken(token) {

        return HttpClient.get("http://localhost:8765/uaa/users/current",{
            headers : {
                Authorization : token
            }
        })
    }

    
}