import * as express from 'express';
import { controller, httpGet, httpPost, httpPut, httpDelete } from 'inversify-express-utils';
import { injectable, inject } from 'inversify';
import { TaskService } from "../services/TaskService"
import TYPES from '../constant/types';
import { Request, Response } from 'express'
import { Auth } from '../middleware/AuthMiddleware';

@injectable()
@controller("/taskmanager_municipality/task")
export class TaskController {

    constructor( @inject(TYPES.TaskService) private taskService: TaskService) { }


    @httpGet("/",Auth({ role : 768}))
    public getAll(request : Request,response : Response): void {
         this.taskService.getAll().then(result =>{
            response.send(result);
        }).catch(error => {
            response.send(error);
        });
    }

    @httpGet("/byOrg/:orgId", Auth({ role : 768}))
    public getByOrgId(request: Request, response : Response): void {

        this.taskService.getByOrgId(request.params.orgId).then(result =>{
            response.send(result);
        });
       
    }

    @httpGet("/byUser/:orgId", Auth({ role : 512}))
    public getByUserId(request: Request, response : Response): void {

        this.taskService.getByOrgId(request.params.orgId).then(result =>{
            response.send(result);
        });
       
    }

    @httpGet('/:id', Auth({ role : 512}))
    public getById(request: Request,response : Response): void {

    
        this.taskService.getById(request.params.id).then(result =>{
            if(result) {
                response.send(result);
            } else {
                response.sendStatus(404);
            }
        }).catch(reason=> {
            response.sendStatus(500);
        })
    }

    @httpPost("/", Auth({ role : 512}))
    public add(request: Request, response: Response): void {

  
        this.taskService.add(request.body).then(result => {
            response.send(result);
        }).catch(error=> {

            response.sendStatus(500);
        });
    }
    @httpPut("/", Auth({ role : 512}))
    public update(request: Request, response: Response): void {
        this.taskService.update(request.body._id,request.body).then(result => {
            if(result) {
                if(Array.isArray(result)) {
                    response.send(result[0])
                } else {
                    response.send(result);
                }
            }
           response.sendStatus(400);
        })
        
    }
    @httpDelete("/:id", Auth({ role : 512}))
    public delete(request: Request,response : Response): void {
        console.log("Delete");
        this.taskService.delete(request.params.id);
   
        response.sendStatus(200);
    }






}

