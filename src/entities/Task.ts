
import { injectable } from 'inversify';
import { ObjectId } from 'mongodb'
import { AttachmentController } from '../controllers/AttachmentController';
import { Attachment } from '../entities/Attachment';

@injectable()
export class Task {
    constructor(
                private orgId : string,
                private name : string,
                private userId : string,
                private _id : ObjectId,
                private lat : number,
                private lng : number,
                private attachments : Array<Attachment>
                
               ) {

                }
    get getId() : ObjectId {
        return new ObjectId(this._id)
    }

    get getOrgId() : string {
        return this.orgId;
    }

    get getName() : string {
        return this.name;
    }
    get getAttachments() : any {
        return this.attachments;
    }
}

