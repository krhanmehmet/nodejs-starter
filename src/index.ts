
import 'reflect-metadata';
import * as express  from 'express'
import { interfaces, InversifyExpressServer, TYPE } from 'inversify-express-utils';
import { Container } from 'inversify';
import * as bodyParser from 'body-parser';
import TYPES from './constant/types';
import { TaskController } from './controllers/TaskController'
import { TaskService } from './services/TaskService'
import { TaskRepository } from './repositories/TaskRepository'






let container = new Container();


container.bind<interfaces.Controller>(TYPE.Controller).to(TaskController).whenTargetNamed("TaskController")
container.bind<TaskService>(TYPES.TaskService).to(TaskService);
container.bind<TaskRepository>(TYPES.TaskRepository).to(TaskRepository);

// start the server
let server = new InversifyExpressServer(container);
server.setConfig((app) => {
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
});

let app = server.build();
app.listen(process.env.PORT || 3033);
//app.listen(3002);
console.log('Server started on port '+ process.env.PORT);

