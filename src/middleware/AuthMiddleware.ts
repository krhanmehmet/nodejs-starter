
import * as express  from 'express';

import { AuthServerClient } from '../components/AuthServerClient';



function hasAccess(user : any , config : any) {

    if(user.principal.roles.taskmanager) {

        if(user.principal.roles.taskmanager.permission >= config.role) {
            return true;
        }
    } 
    return false;
}


function AuthMiddleware() {
    return (config: { scope? : string, role? : number }) => {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            if(!req.headers.authorization) {
                res.sendStatus(401);
            }

            console.log(req);
            (async () => {
                AuthServerClient.getUserByToken(req.headers.authorization).then(data=> {
                    if(hasAccess(data.data,config)) {
                        next();
                    } else {
                    
                        res.sendStatus(401);
                    }
                }).catch(error=> {
                    res.sendStatus(401);
                })
            })();
        };
    };
}

const Auth = AuthMiddleware();

export { Auth };
