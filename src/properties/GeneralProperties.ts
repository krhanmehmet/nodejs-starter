


export class GeneralProperties {



    public static getMongoURI() : string {
        return process.env.MONGO_URI || "mongodb://parabol:parabol@localhost:27017/taskmanager-new"
    }

    public static getMongoUserName() : string {
        return process.env.MONGO_USERNAME;
    }
    public static getMongoPassword() : string {
        return process.env.MONGO_PASSWORD;
    }
    public static getMongoHost() : string {
        return process.env.MONGO_HOST;
    }
    public static getMongoPort() : string {
        return process.env.MONGO_PORT;
    }

}