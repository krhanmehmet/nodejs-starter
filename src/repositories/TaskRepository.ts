
import * as mongoose from 'mongoose'
import { injectable } from 'inversify'
import { Task } from '../entities/Task'
import { MongoDBClient } from '../utils/client'






@injectable()
export class TaskRepository {

    client : MongoDBClient<Task>;
    
    constructor()
     {
        this.client = new MongoDBClient<Task>();
     }
    
    public getAll() : Promise<any[]> {
                   
        return new Promise<any[]>(( resolve,reject) => {
            this.client.find("Task",{},(error,data : any []) => {
                resolve(data);
            });
        });
    }

    public getById(id) : Promise<any> {
        return new Promise<any>(( resolve,reject) => {
            this.client.findOneById("Task", id,(error,data : any)=>{
                if(error) {
                    reject(error);
                }
                resolve(data[0]);
            });
        })
    }
    public getByOrgId(orgId) : Promise<any[]> {

        return new Promise<any[]>(( resolve,reject) => {


            this.client.find("Task", { orgId : orgId},(error,data : any[])=>{
                resolve(data);
            });
        })
      
    }

    public getByUserId(userId) : Promise<any[]> {
        
                return new Promise<any[]>(( resolve,reject) => {
        
        
                    this.client.find("Task", { userId : userId},(error,data : any[])=>{
                        resolve(data);
                    });
                })
              
    }
    public add(task : any) : Promise<Task> {

        return new Promise<any>((resolve, reject) => {
            this.client.insert('Task', task, (error, data: any) => {
              resolve(data);
            });
          });

    }
    public update(id : string,task : any) : Promise<Task> {
        return new Promise<Task>(( resolve,reject) => {
            this.client.update('Task',id,task, (error, data: any) => {
                resolve(data);
              });
        });
       
    }
    
    public delete(id) : void {
      this.client.remove("Task",id,null);
    }



}



