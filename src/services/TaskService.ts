

import { injectable,inject } from 'inversify'
import { Task } from '../entities/Task'
import { TaskRepository } from '../repositories/TaskRepository'

import TYPES from '../constant/types';



@injectable() 
export class TaskService {

    
   
    constructor(@inject(TYPES.TaskRepository) private taskRepository : TaskRepository) {}

    public getAll() : Promise<Task[]> { 
        return this.taskRepository.getAll();
    }
    public getById(id : string) : Promise<Task> {
        return this.taskRepository.getById(id);   
    }

    public getByOrgId(orgId : any) : Promise<Task[]> {
        return this.taskRepository.getByOrgId(orgId);
    }
    public add(task : any) : Promise<Task> {
        
        return this.taskRepository.add(task);
      
    }
    public update(id : string,task : Task) : Promise<Task> {
        return this.taskRepository.update(id,task);
    }

    public delete(id : string) : void {
         this.taskRepository.delete(id);
     

    }

}