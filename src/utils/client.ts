import { Db, ObjectID } from 'mongodb';
import { injectable } from 'inversify';
import { MongoDBConnection } from './connection';

@injectable()
export class MongoDBClient<T> {
  public db: Db;

  constructor() {
    MongoDBConnection.getConnection((connection) => {
      this.db = connection;
      console.log("DB connection established");
    });
  }

  public find(collection: string, filter: Object, result: (error,data)=> void): void {
    console.log("Client find");
      this.db.collection(collection).find(filter).toArray((error,data) => {
        return result(error,data);
      });
  }

  public findOneById(collection: string, objectId: string, result: (error,data) => void): void{
    this.db.collection(collection).find({ _id: new ObjectID(objectId) }).limit(1).toArray((error, find) => {
      return result(error,find);
    });
  }

  public insert(collection: string, model: T, result : (error,data) => void): void {
    this.db.collection(collection).insertOne(model, (error, insert) => {
      return result(error, insert.ops[0]);
    });
  }

  public  update(collection: string, objectId: string, model: any, result: (error,data)=> void ): void {
    delete model._id;
    this.db.collection(collection).updateOne({ _id: new ObjectID(objectId) }, model, (error, update) => {
      console.log(error);
      return result(error,update);
    });
  
  }

  public remove(collection: string, objectId: string, result: (error, data) => void): void {
    this.db.collection(collection).deleteOne({ _id: new ObjectID(objectId) }, (error, remove) => {
      if(result != null) {
        return result(error, remove);
      }

      return null;
     
    });
  }
}