import { Db, MongoClient } from 'mongodb';
import { GeneralProperties } from '../properties/GeneralProperties';


 //const url: string = 'mongodb://parabol:parabol@localhost:27017/iotcentral';

export class MongoDBConnection {
  private static isConnected: boolean = false;
  private static db: Db;
  private static url : string;

  public static getConnection(result: (connection) => void) {
    this.url = GeneralProperties.getMongoURI();

    if (this.isConnected) {

      return result(this.db);
      
    } else {
      this.connect((error, db: Db) => {
        return result(this.db);
      });
    }
  }

  private static connect(result: (error, db: Db) => void) {

    console.log(process.env.MONGO_URI);
    MongoClient.connect(GeneralProperties.getMongoURI(), (error, db: Db) => {
      if(error) {
        console.log("Error:",error);
      }
      this.db = db;
      this.isConnected = true;

      return result(error, db);
    });
  }
}