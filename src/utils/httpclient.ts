import axios from 'axios';




export class HttpClient{

    constructor() {

    }

    public static get(url : string, config? : any) : Promise<any> {
        url = HttpClient.urlComposer(url);
        console.log(config);
        return axios.get(url,config);
    }

    public static post(url,data,headers) : Promise<any>{
        url = HttpClient.urlComposer(url);
        return axios.post(url,data,headers)
    }

    public static put(url,header,data) : Promise<any> {
        return;
    }
    public static delete(url) : Promise<any> {
        return;
    }

    public static request(config) : Promise<any> {
        config.url = HttpClient.urlComposer(config.url);
        return axios.request(config);
    }
    private static urlComposer(url : string) : string {
        if(!url.startsWith("http://")) {
            url = "http://" + url;
        }

        return url;
    }


}